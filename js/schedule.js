$(document).ready(function() {
    $(".get-schedule").click(function() {
        var getStreet1 = $('.street1').val();
        var getStreet2 = $('.street2').val();
        $(".schedule-content").html('<div class="ajax-loader"><img src="images/ajax-loader.gif"/></div>');
        $.post("schedule.php",
        {
            street1: getStreet1,
            street2: getStreet2
        }, function(result){
            $(".schedule-content").html(result);
        });
    });
});