<html>
<head>
    <title>TTC App</title>
    <meta name="viewport" content="width=320, initial-scale=1, maximum-scale=1, user-scalable=yes">
    <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../bootstrap/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="css/style.css">
    <script src="../bootstrap/js/jquery.min.js"></script>
    <script src="../bootstrap/js/bootstrap.min.js"></script>
    <script src="js/jquery.min.js"></script>
    <script src="js/schedule.js"></script>
</head>
<body>
    <div class="container">
        <div class="header">
            <div class="bg-primary">
                <div class="row">
                    <div class="col-xs-1"></div>
                    <div class="col-xs-10"><h2>TTC Schedule</h2></div>
                    <div class="col-xs-1"></div>
                </div>
            </div>
            <div class="sub-header bg-info">
                <div class="row">
                    <div class="col-xs-1"></div>
                    <div class="col-xs-10"><h3>Enter Intersection</h3></div>
                    <div class="col-xs-1"></div>
                </div>
                <div class="row">
                    <div class="col-xs-1"></div>
                    <div class="col-xs-10">
                        <div class="form-group">
                            <label for="street1Input">Street 1</label>
                            <input type="text" name="street1" value="" placeholder="Enter Street 1" class="form-control input-lg street1">
                        </div>
                    </div>
                    <div class="col-xs-1"></div>
                </div>
                <div class="row">
                    <div class="col-xs-1"></div>
                    <div class="col-xs-10">
                        <div class="form-group">
                            <label for="street2Input">Street 2</label>
                            <input type="text" name="street2" value="" placeholder="Enter Street 2" class="form-control input-lg street2">
                        </div>
                    </div>
                    <div class="col-xs-1"></div>
                </div>
                <div class="row">
                    <div class="col-xs-1"></div>
                    <div class="col-xs-10">
                        <input type="button" class="btn btn-md btn-primary get-schedule" value="CHECK BUS SCHEDULE">
                    </div>
                    <div class="col-xs-1"></div>
                </div>
                <div class="row">&nbsp;</div>
            </div>
        </div>
        <div class="row">&nbsp;</div>
        <div class="schedule-content"></div>
        <div class="row">&nbsp;</div>
    </div>
</body>
</html>