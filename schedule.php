<?php
    if(((isset($_POST['street1'])) && ($_POST['street1'] != '')) && ((isset($_POST['street2'])) && ($_POST['street2'] != ''))) {
        $street1 = str_replace(' ','_',$_POST['street1']);
        $street2 = str_replace(' ','_',$_POST['street2']);
        $schedule = locate($street1, $street2);
    } else {
        $html = '<h2>Please enter an intersection</h2>';
    }

    function getJson($street1, $street2, $counter) {
        $url = 'http://myttc.ca/' . $street1 . '_and_' .$street2 . '.json';
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_TIMEOUT, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $data = curl_exec($ch);
        curl_close($ch);
        $ttcJson = json_decode($data, true);
        if($ttcJson == NULL) {
			if($counter == 0) {
				$counter++;
				$ttcJson = getJson($street2, $street1, $counter);
			}
        }
        return $ttcJson;
    }

    function locate($street1, $street2) {
        $ttcJson = getJson($street1, $street2, 0);
		$html = '';
		if($ttcJson != NULL) {
			date_default_timezone_set('America/Toronto');
			$html = '<div class="row">';
			$html .= '<div class="col-xs-1"></div>';
			$html .= '<div class="col-xs-11">';
			$html .= '<div class="col-xs-4">Searched: </div><div class="body col-xs-7">' . strtoupper(date("m/d/Y g:i a")) . '</div>';
			$html .= '</div>';
			$html .= '</div>';
			$html .= '<div class="row">&nbsp;</div>';
			if(is_array($ttcJson)){
				foreach( $ttcJson as $ttcKey => $ttcValue ) {
					if(is_array($ttcValue)) {
						foreach( $ttcValue as $key => $val ) {
							$html .= '<div class="row">';
							$html .= '<div class="col-xs-1"></div>';
							$html .= '<div class="col-xs-11">';
							$html .= '<div class="title col-xs-4">Intersection: </div><div class="body col-xs-7">' . $val['name'] . '</div>';
							$html .= '</div>';
							$html .= '</div>';
							foreach($val['routes'] as $keyRoutes => $valRoutes) {
								$html .= '<div class="row">&nbsp;</div>';
								$html .= '<div class="row">';
								$html .= '<div class="col-xs-1"></div>';
								$html .= '<div class="col-xs-11">';
								$html .= '<div class="title col-xs-4">Bus Name: </div><div class="body col-xs-7">' .  ucwords(str_replace('_', ' ', $valRoutes['uri'])) . '</div>';
								$html .= '</div>';
								$html .= '</div>';
								$html .= '<div class="row">';
								$html .= '<div class="col-xs-1"></div>';
								$html .= '<div class="col-xs-11">';
								$html .= '<div class="title col-xs-4">Route: </div><div class="body col-xs-7">' . $valRoutes['stop_times'][0]['shape'] . '</div>';
								$html .= '</div>';
								$html .= '</div>';

								foreach($valRoutes['stop_times'] as $keyStopTimes => $valStopTimes) {
									$html .= '<div class="row">';
									$html .= '<div class="col-xs-1"></div>';
									$html .= '<div class="col-xs-11">';
									$html .= '<div class="title col-xs-4">Arrival: </div><div class="body col-xs-7">' . date('m/d/Y', $valStopTimes['departure_timestamp']) . ' - ' . str_replace('a',' AM',str_replace('p',' PM',$valStopTimes['departure_time'])) .  '</div>';
									$html .= '</div>';
									$html .= '</div>';
								}
							}
						}
					}
				}
			} 
		} else {
			$html .= '<div class="row">';
			$html .= '<div class="col-xs-1"></div>';
			$html .= '<div class="col-xs-11">';
			$html .= '<h1>No Results</h1><h3>Please check spelling';
			$html .= '</div>';
			$html .= '</div>';
		}
        echo $html;
    }
?>